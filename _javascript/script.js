var slide_atual = 0;
window.onload = function() {
	var i = 0;
	setInterval(passarSlides, 5000);
	var slidewidth = document.getElementById("slider").offsetWidth;
	var objs = document.getElementsByClassName("slide");
	for(i = 0; i < objs.length; i ++) {
		objs[i].style.width = slidewidth + 'px';
	}

}

function passarSlides() {
	var slidewidth = document.getElementById("slider").offsetWidth;

	//Alterar o valor 3 depois que adicionar mais slides, pois ele se refere a quantidade de slides que existem.
	if (slide_atual >= 3) {
		slide_atual = 0;
	} else {
		slide_atual ++;
	}

	document.getElementsByClassName("slidearea")[0].style.marginLeft = '-' + (slidewidth * slide_atual) + 'px'; 

}